from typing import List

import numpy as np

from critical_manifold import CriticalManifold

from matplotlib import pyplot as plt
import plotly.graph_objects as go

Figure = go.Figure
Scatter3d = go.Scatter3d
Line = go.scatter3d.Line
Layout = go.Layout


def create_interactive_plot(critical_manifold: CriticalManifold, system_coordinates: List[np.ndarray]) -> Figure:

    fig_data = []

    # add critical manifold to plot
    for i, branch in enumerate(critical_manifold.branches):
        if i == 0:
            show_legend = True
        else:
            show_legend = False

        attracting_part = Scatter3d(x=branch.x.attracting,
                                    y=branch.y.attracting,
                                    z=branch.w.attracting,
                                    mode='lines',
                                    showlegend=show_legend,
                                    legendgroup='critical manifold (attracting)',
                                    name='critical manifold (attracting)',
                                    line=Line(color='#0000ff',
                                              dash='solid')
                                    )
        repelling_part = Scatter3d(x=branch.x.repelling,
                                   y=branch.y.repelling,
                                   z=branch.w.repelling,
                                   mode='lines',
                                   showlegend=show_legend,
                                   legendgroup='critical manifold (repelling)',
                                   name='critical manifold (repelling)',
                                   line=Line(color='#ff0000',
                                             dash='dash')
                                   )

        fig_data.extend([attracting_part, repelling_part])

    unphysical_part = Scatter3d(x=critical_manifold.branch2.x.unphysical,
                                y=critical_manifold.branch2.y.unphysical,
                                z=critical_manifold.branch2.w.unphysical,
                                mode='lines',
                                showlegend=True,
                                legendgroup='critical manifold (repelling)',
                                name='critical manifold (unphysical repelling)',
                                line=Line(color='#808080',
                                          dash='dash')
                                )

    fig_data.append(unphysical_part)

    # add simulated system to plot
    x_1 = np.array([y_e[0] for y_e in system_coordinates])
    y_1 = np.array([y_e[1] for y_e in system_coordinates])
    x_2 = np.array([y_e[2] for y_e in system_coordinates])
    y_2 = np.array([y_e[3] for y_e in system_coordinates])
    X = (x_1 + x_2) * 0.5
    Y = (y_1 + y_2) * 0.5
    w = np.array([y_e[4] for y_e in system_coordinates])

    colors = ['#000000', '#00ffff', '#ff9900']
    names = ['Laser mean', 'Laser 1', 'Laser 2']

    for x, y, w, color, name in zip([X, x_1, x_2], [Y, y_1, y_2], [w, w, w], colors, names):
        fig_data.append(Scatter3d(x=x,
                                  y=y,
                                  z=w,
                                  mode='lines',
                                  showlegend=True,
                                  name=name,
                                  line=Line(color=color,
                                            dash='solid')
                                  )
                        )

    # add layout specifications
    fig_layout = Layout(arg={'title': 'Simulation of two mean-field coupled lasers',
                             'scene': {'xaxis': {'title': 'X'},
                                       'yaxis': {'title': 'Y'},
                                       'zaxis': {'title': 'w'}
                                       }
                             }
                        )

    return Figure(data=fig_data, layout=fig_layout)


def create_2d_projection_plots(critical_manifold: CriticalManifold, system_coordinates: List[np.ndarray],
                               system_time: List[float]):
    # TODO refactor
    x_1 = np.array([y_e[0] for y_e in system_coordinates])
    y_1 = np.array([y_e[1] for y_e in system_coordinates])
    x_2 = np.array([y_e[2] for y_e in system_coordinates])
    y_2 = np.array([y_e[3] for y_e in system_coordinates])
    X = (x_1 + x_2) * 0.5
    Y = (y_1 + y_2) * 0.5
    w = np.array([y_e[4] for y_e in system_coordinates])

    t = np.array([system_time]).reshape(-1)

    # only critical manifold
    fig, ax = plt.subplots()

    for i, branch in enumerate(critical_manifold.branches):
        if i == 0:
            line_attracting, = ax.plot(branch.x.attracting, branch.w.attracting, '-b',
                                       label='critical manifold (attracting)')
            line_repelling, = ax.plot(branch.x.repelling, branch.w.repelling, '--r',
                                      label='critical manifold (repelling)')
        else:
            ax.plot(branch.x.attracting, branch.w.attracting, '-b')
            ax.plot(branch.x.repelling, branch.w.repelling, '--r')

    unphysical_x = critical_manifold.branch2.x.unphysical
    unphysical_w = critical_manifold.branch2.w.unphysical
    unphysical_line, = ax.plot(unphysical_x, unphysical_w, ls='--', color='#808080', lw=0.75,
                               label='critical manifold \n(repelling unphysical part)')

    ax.set_xlim(-0.01, 1.8)
    ax.set_ylim(-0.9, -0.3)

    ax.set_xlabel('X')
    ax.set_ylabel('w')

    ax.legend(handles=[line_attracting, line_repelling, unphysical_line])

    fig.show()
    fig.savefig('critical_manifold.pdf')

    # critical manifold with lasers
    fig, ax = plt.subplots()

    mean_line, = ax.plot(X, w, '-k', label='Laser mean')

    for i, branch in enumerate(critical_manifold.branches):
        if i == 0:
            line_attracting, = ax.plot(branch.x.attracting, branch.w.attracting, '-b',
                                       label='critical manifold (attracting)')
            line_repelling, = ax.plot(branch.x.repelling, branch.w.repelling, '--r',
                                      label='critical_manifold (repelling)')
        else:
            ax.plot(branch.x.attracting, branch.w.attracting, '-b')
            ax.plot(branch.x.repelling, branch.w.repelling, '--r')

    ax.set_xlim(-0.05, 7.5)
    ax.set_ylim(-0.9, -0.3)

    ax.set_xlabel('X')
    ax.set_ylabel('w')

    ax.legend(handles=[line_attracting, line_repelling, mean_line])

    fig.show()
    fig.savefig('simulation.pdf')

    # x_i over time
    fig, ax = plt.subplots()

    line2, = ax.plot(t, x_2, '-g', label='Laser 2')
    line1, = ax.plot(t, x_1, '-c', label='Laser 1')

    ax.legend(handles=[line1, line2])

    max_x_2 = max(x_2)
    ax.set_ylim(0, 2.2 * max_x_2)
    ax.set_xlim(6500, 17500)

    ax.set_xlabel('t')
    ax.set_ylabel(r'$x_i$')

    fig.show()
    fig.savefig('lasers.pdf')
