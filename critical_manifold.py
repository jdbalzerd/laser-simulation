from typing import Tuple, NamedTuple, Optional, List

from abc import ABC, abstractmethod

import numpy as np

from scipy.optimize import fsolve


class BranchCoordinates(NamedTuple):
    all: Optional[np.ndarray] = None
    attracting: Optional[np.ndarray] = None
    repelling: Optional[np.ndarray] = None
    unphysical: Optional[np.ndarray] = None


class Branch(ABC):

    def __init__(self, delta_1: float, delta_2: float, k: float, w_range: Tuple[float, float]):
        self.d_1 = min(delta_1, delta_2)
        self.d_2 = max(delta_1, delta_2)
        self.k = k
        self.w_range = w_range

        self._attracting_condition: Optional[np.ndarray] = None

        self._x: Optional[BranchCoordinates] = None
        self._y: Optional[BranchCoordinates] = None
        self._w: Optional[BranchCoordinates] = None

    @abstractmethod
    def get_attracting_condition(self, *kwargs) -> np.ndarray:
        pass

    @abstractmethod
    def calculate_branch(self):
        pass

    @property
    def x(self) -> BranchCoordinates:
        if self._x is None:
            self.calculate_branch()
        return self._x

    @property
    def y(self) -> BranchCoordinates:
        if self._y is None:
            self.calculate_branch()
        return self._y

    @property
    def w(self) -> BranchCoordinates:
        if self._w is None:
            self.calculate_branch()
        return self._w


class Branch1(Branch):

    def __init__(self, delta_1: float, delta_2: float, k: float, w_range: Tuple[float, float]):
        super().__init__(delta_1, delta_2, k, w_range)

    def calculate_all_w(self, num: int = 600) -> np.ndarray:
        return np.linspace(start=self.w_range[0], stop=self.w_range[1], num=num, endpoint=True)

    def get_attracting_condition(self, all_w: np.ndarray) -> np.ndarray:
        return np.where(all_w < (1 - self.d_2) / self.k, 1, 0)

    def calculate_branch(self):
        all_w = self.calculate_all_w()
        all_x = np.zeros(len(all_w))
        all_y = (self.d_1 + self.d_2)/2 + self.k * all_w

        self._attracting_condition = self.get_attracting_condition(all_w)

        self._x = BranchCoordinates(all=all_x, attracting=all_x[self._attracting_condition == 1],
                                    repelling=all_x[self._attracting_condition == 0])
        self._y = BranchCoordinates(all=all_y, attracting=all_y[self._attracting_condition == 1],
                                    repelling=all_y[self._attracting_condition == 0])
        self._w = BranchCoordinates(all=all_w, attracting=all_w[self._attracting_condition == 1],
                                    repelling=all_w[self._attracting_condition == 0])


class Branch2(Branch):

    def __init__(self, delta_1: float, delta_2: float, k: float, a: float, alpha: float, w_range: Tuple[float, float],
                 tol: float = 1.5e-3):
        super().__init__(delta_1, delta_2, k, w_range)
        self.a = a
        self.alpha = alpha
        self.tol = tol

    def implicit_x_func(self, x: float, w: float) -> float:
        f = self.a * np.log(1 + self.alpha * x)
        return (self.d_1 + self.d_2) / 2 - 1 + self.k * (w + f) - x

    def calculate_all_w(self, num: int = 600) -> np.ndarray:
        part_w = np.linspace(start=self.w_range[0], stop=self.w_range[1], num=num, endpoint=True)
        return np.concatenate([part_w] * 2)

    def calculate_all_x(self, all_w: np.ndarray, x_00: float = 0.25, x_01: float = 0.75) -> np.ndarray:
        all_x = np.array([np.nan] * len(all_w))

        for i in range(len(all_x)//2):
            j = i + len(all_x)//2

            x_fix_point_1 = fsolve(self.implicit_x_func, args=(all_w[i]), x0=np.array(x_00))
            x_fix_point_2 = fsolve(self.implicit_x_func, args=(all_w[j]), x0=np.array(x_01))

            evaluated_func1 = self.implicit_x_func(x_fix_point_1, all_w[i])
            evaluated_func2 = self.implicit_x_func(x_fix_point_2, all_w[j])

            if abs(evaluated_func1) <= self.tol:
                all_x[i] = x_fix_point_1
            if abs(evaluated_func2) <= self.tol:
                all_x[j] = x_fix_point_2

        return all_x

    def get_attracting_condition(self, all_x: np.ndarray):
        x_attracting_value = self.k * self.a - 1 / self.alpha
        return np.where(all_x > x_attracting_value, 1, 0)

    def calculate_branch(self):
        all_w = self.calculate_all_w()
        all_x = self.calculate_all_x(all_w=all_w)
        all_y = np.ones(len(all_x))

        all_w = all_w[all_x >= 0]
        all_y = all_y[all_x >= 0]
        all_x = all_x[all_x >= 0]

        self._attracting_condition = self.get_attracting_condition(all_x=all_x)

        self._x = BranchCoordinates(all=all_x, attracting=all_x[self._attracting_condition == 1],
                                    repelling=all_x[self._attracting_condition == 0])
        self._y = BranchCoordinates(all=all_y, attracting=all_y[self._attracting_condition == 1],
                                    repelling=all_y[self._attracting_condition == 0])
        self._w = BranchCoordinates(all=all_w, attracting=all_w[self._attracting_condition == 1],
                                    repelling=all_w[self._attracting_condition == 0])


class Branch3(Branch):

    def __init__(self, delta_1: float, delta_2: float, k: float, a: float, alpha: float, w_range: Tuple[float, float],
                 tol: float = 1.5e-3):
        super().__init__(delta_1, delta_2, k, w_range)
        self.a = a
        self.alpha = alpha
        self.tol = tol

    def implicit_x_func(self, x: float, w: float) -> float:
        f = self.a * np.log(1 + self.alpha/2 * x)
        return self.d_2 - 1 + self.k*(w + f) - x

    def calculate_all_w(self, num: int = 1000) -> np.ndarray:
        return np.linspace(start=self.w_range[0], stop=self.w_range[1], num=num, endpoint=True)

    def calculate_all_x(self, all_w: np.ndarray, x_0=0.5) -> np.ndarray:
        all_x = np.array([np.nan] * len(all_w))

        for i in range(len(all_x)):

            x_fix_point = fsolve(self.implicit_x_func, args=(all_w[i]), x0=np.array(x_0))
            evaluated_func = self.implicit_x_func(x_fix_point, all_w[i])

            if abs(evaluated_func) <= self.tol:
                all_x[i] = x_fix_point

        return all_x

    def calculate_all_y(self, all_x: np.ndarray, all_w: np.ndarray) -> np.ndarray:
        f = self.a * np.log(1 + self.alpha * all_x)
        all_y = 1 / 2 * (self.d_1 + self.k * (all_w + f) + 1)

        return all_y

    def get_attracting_condition(self, all_x: np.ndarray, all_y: np.ndarray) -> np.ndarray:
        x_attracting_value = (self.k * self.a) / 2 - 1 / self.alpha
        return np.logical_and(all_x > x_attracting_value, all_y < 1)

    def calculate_branch(self):
        all_w = self.calculate_all_w()
        all_x = self.calculate_all_x(all_w=all_w)
        all_y = self.calculate_all_y(all_x=all_x, all_w=all_w)

        all_w = all_w[all_x >= 0]
        all_y = all_y[all_x >= 0]
        all_x = all_x[all_x >= 0]

        self._attracting_condition = self.get_attracting_condition(all_x=all_x, all_y=all_y)

        self._x = BranchCoordinates(all=all_x, attracting=all_x[self._attracting_condition == 1],
                                    repelling=all_x[self._attracting_condition == 0])
        self._y = BranchCoordinates(all=all_y, attracting=all_y[self._attracting_condition == 1],
                                    repelling=all_y[self._attracting_condition == 0])
        self._w = BranchCoordinates(all=all_w, attracting=all_w[self._attracting_condition == 1],
                                    repelling=all_w[self._attracting_condition == 0])


class Branch4(Branch3):

    def __init__(self, delta_1: float, delta_2: float, k: float, a: float, alpha: float, w_range: Tuple[float, float],
                 tol: float = 1.5e-3):
        super().__init__(delta_1, delta_2, k, a, alpha, w_range, tol)
        self.d_1, self.d_2 = self.d_2, self.d_1


class CriticalManifold:

    def __init__(self, delta_1: float, delta_2: float, k: float, a: float, alpha: float, w_range: Tuple[float, float],
                 tol: float = 1e-3):
        self.branch1 = Branch1(delta_1=delta_1, delta_2=delta_2, k=k, w_range=w_range)
        self.branch2 = Branch2(delta_1=delta_1, delta_2=delta_2, k=k, a=a, alpha=alpha, w_range=w_range, tol=tol)
        self.branch3 = Branch3(delta_1=delta_1, delta_2=delta_2, k=k, a=a, alpha=alpha, w_range=w_range, tol=tol)
        self.branch4 = Branch4(delta_1=delta_1, delta_2=delta_2, k=k, a=a, alpha=alpha, w_range=w_range, tol=tol)

        self.set_unphysical_part_of_branch3()

    def set_unphysical_part_of_branch3(self):

        max_repelling_w = max(self.branch3.w.attracting)

        unphysical_w = self.branch2.w.repelling[self.branch2.w.repelling > max_repelling_w]
        unphysical_x = self.branch2.x.repelling[self.branch2.w.repelling > max_repelling_w]
        unphysical_y = self.branch2.y.repelling[self.branch2.w.repelling > max_repelling_w]

        physical_w = self.branch2.w.repelling[self.branch2.w.repelling <= max_repelling_w]
        physical_x = self.branch2.x.repelling[self.branch2.w.repelling <= max_repelling_w]
        physical_y = self.branch2.y.repelling[self.branch2.w.repelling <= max_repelling_w]

        self.branch2._w = BranchCoordinates(all=self.branch2.w.all, repelling=physical_w,
                                            attracting=self.branch2.w.attracting, unphysical=unphysical_w)
        self.branch2._x = BranchCoordinates(all=self.branch2.x.all, repelling=physical_x,
                                            attracting=self.branch2.x.attracting, unphysical=unphysical_x)
        self.branch2._y = BranchCoordinates(all=self.branch2.y.all, repelling=physical_y,
                                            attracting=self.branch2.y.attracting, unphysical=unphysical_y)

    @property
    def branches(self) -> List[Branch]:
        return [self.branch1, self.branch2, self.branch3, self.branch4]
