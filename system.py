import numpy as np


class System:

    def __init__(self, delta_1: float, delta_2: float, k: float, alpha: float, a: float, gamma: float, epsilon: float):
        self.d_1 = min(delta_1, delta_2)
        self.d_2 = max(delta_1, delta_2)
        self.k = k
        self.alpha = alpha
        self.a = a
        self.gamma = gamma
        self.epsilon = epsilon

    def __call__(self, t, y: np.ndarray):
        x_1 = y[0]
        y_1 = y[1]
        x_2 = y[2]
        y_2 = y[3]
        w = y[4]

        f = self.a * np.log(1 + self.alpha * 0.5 * (x_1 + x_2))

        new_x_1 = x_1 * (y_1 - 1)
        new_y_1 = self.gamma * (self.d_1 - y_1 + self.k * (w + f) - x_1 * y_1)
        new_x_2 = x_2 * (y_2 - 1)
        new_y_2 = self.gamma * (self.d_2 - y_2 + self.k * (w + f) - x_2 * y_2)
        new_w = -1 * self.epsilon * (w + f)

        return np.array([new_x_1, new_y_1, new_x_2, new_y_2, new_w])
