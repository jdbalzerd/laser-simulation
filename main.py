from typing import List, Tuple

import numpy as np

from scipy.integrate import RK45

from system import System
from critical_manifold import CriticalManifold
from helper import create_interactive_plot, create_2d_projection_plots


def run_simulation(y_start: np.ndarray, system: System, t_bound: float, max_step: float) \
        -> Tuple[List[np.ndarray], List[float]]:
    t0 = 0
    rk45 = RK45(fun=system, t0=t0, y0=y_start, t_bound=t_bound, max_step=max_step)

    y = [y_start]
    y_t = [t0]

    while rk45.status != 'finished':
        y.append(rk45.y)
        y_t.append(rk45.t)
        rk45.step()

    return y, y_t


def main():
    # config
    delta_1, delta_2 = 1.35, 1.45
    k = 0.7
    alpha = 2
    a = 1/k
    gamma = 4e-3
    epsilon = 1e-4
    w_range = (-0.3, -0.9)

    max_step = 0.1
    t_bound = 20000
    y_start = np.array([0.1, 0.57, 0.1, 0.38, -0.9])

    # calculations
    critical_manifold = CriticalManifold(delta_1=delta_1, delta_2=delta_2, k=k, a=a, alpha=alpha, w_range=w_range)

    system = System(delta_1=delta_1, delta_2=delta_2, k=k, alpha=alpha, a=a, gamma=gamma, epsilon=epsilon)

    system_y, system_t = run_simulation(y_start=y_start, system=system, t_bound=t_bound, max_step=max_step)

    # plot
    fig = create_interactive_plot(critical_manifold=critical_manifold, system_coordinates=system_y)
    fig.show()


if __name__ == '__main__':
    main()
